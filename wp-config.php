<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bouwtekenaar_wp630' );

/** Database username */
define( 'DB_USER', 'bouwtekenaar_wp630' );

/** Database password */
define( 'DB_PASSWORD', '8@kSb1T]7p' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aghdkogoahamoqmfypag1xos3d98c0pefu3hkjpncc1uzxvhsfjnihncuiijbmtp' );
define( 'SECURE_AUTH_KEY',  'kljwjz4q9vc3rwq3zsup6xl9aqorh1hd51bamg4egjut9obmxxbr7ebyqckx1rlx' );
define( 'LOGGED_IN_KEY',    'hxudbbvbfnljgm5y3p0evxsj7tc9s1fuz9pporpln8mdvrxzepd4fq5nfmbizieb' );
define( 'NONCE_KEY',        'hawzqu3g0t2ghubjhahgrkkgoxforutwhu6sceyqgkgftmtrbm21vruf5bfy7mzq' );
define( 'AUTH_SALT',        'bcqxvtgjhuc8g76xkjiauagyjoyu3e0gaubobfxsw7krebcknh68ha3nqg6fxvmt' );
define( 'SECURE_AUTH_SALT', 'zsxba2eb6ceclzefnxjbkmlfvf8c499utie4yaafrujpfhlixz4ak2qra7dfjcre' );
define( 'LOGGED_IN_SALT',   'xr2hlqkqnqtgfnzsrfbyihrf6xw4o41lzimot9h86ow9thzfkjubthrlb0dafrlw' );
define( 'NONCE_SALT',       'izeu84imox7rw3ooa2u0ipj9ajtj7did7hjzls958rc1enf4e3xnsbymdrp0cwej' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp62_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
